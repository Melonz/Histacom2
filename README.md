<a href="https://ci.appveyor.com/project/TheRandomMelon/histacom2"><img src="https://ci.appveyor.com/api/projects/status/87hnar03ayvyaehk?svg=true
" style="border: 0;" alt="AppVeyor Badge"></a>
[![Discord](https://img.shields.io/discord/266018132827570176.svg?colorB=7289DA&label=discord)](https://discord.gg/ffuXR9k)

# Histacom 2
## The remake of the original hacking simulator!
Histacom 2 is a C# version of the PC game Histacom, released by 12padams in 2010. Here are some of our goals:
* Get Histacom 2 to the same playable state that the original Histacom's in and beyond.
* Add more applications to the operating systems.
* Add more story elements.

## Building
Histacom 2 is currently being developed in Microsoft Visual Studio 2017. Visual Studio 2015 has also been proven to work. If you are using a different IDE, please do so with caution. We cannot guarantee the stability of other IDEs.

Load the ``.sln`` project file into Visual Studio and compile. No further steps are required. This list may grow in the future.

## Links

Histacom 2 has a Discord server at https://discord.gg/HAZsB6H (You can also click on the dark blue Discord Badge)

Histacom 2 has an AppVeyor project at https://ci.appveyor.com/project/TheRandomMelon/histacom2 (You can also use the AppVeyor badge)

Histacom 2 has a YouTube channel at https://www.youtube.com/channel/UCU3uiIgHdLaNvPYEB1hWPsg (We have a tutorial on how to contribute over there)

## Documentation
Histacom 2's documentation can be found on the Histacom 2 Wiki: https://gitlab.com/Melonz/Histacom2/wiki

## License
Histacom 2 is licensed under the [MIT license](https://gitlab.com/Melonz/Histacom2/blob/master/LICENSE). All code committed to the Histacom 2 repository becomes the property of Melonz Software, and will **not** be removed upon request.
